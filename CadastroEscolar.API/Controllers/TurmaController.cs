﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CadastroEscolar.API.Domain.Models;
using CadastroEscolar.API.Domain.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CadastroTurmar.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TurmaController : ControllerBase
    {
        private readonly ITurmaService _TurmaService;

        public TurmaController(ITurmaService TurmaService)
        {
            _TurmaService = TurmaService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<IEnumerable<Turma>>> GetAllAsync()
        {
            try
            {
                var personList = await _TurmaService.ListTurmaAsync();

                return Ok(personList);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("Escola/{escolaId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<IEnumerable<Turma>>> GetAllByEscolaAsync([FromRoute] int escolaId)
        {
            try
            {
                var TurmaList = await _TurmaService.ListTurmaByEscolaAsync(escolaId);

                return Ok(TurmaList);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Turma> GetById([FromRoute] int id)
        {
            try
            {
                var person = _TurmaService.GetbyId(id);

                if (person == null)
                    return NotFound("Turma não encontrada!");

                return Ok(person);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<IEnumerable<Turma>> Add([FromBody] Turma Turma)
        {
            try
            {
                var clientPriority = _TurmaService.Add(Turma);

                return Ok(clientPriority);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
