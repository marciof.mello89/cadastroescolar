﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CadastroEscolar.API.Domain.Models
{
    public class Escola
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Range(0, 999999999)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Nome { get; set; }

        [JsonIgnore]
        public IList<Turma> Turmas { get; set; }
    }
}
