﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CadastroEscolar.API.Domain.Models
{
    public class Turma
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Range(0, 999999999)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Nome { get; set; }

        [Range(0, 999999999)]
        public int EscolaId { get; set; }

        [JsonIgnore]
        [ForeignKey("EscolaId")]
        public Escola Escola { get; set; }
    }
}
