﻿using CadastroEscolar.API.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CadastroEscolar.API.Domain.Repositories
{
    public interface IEscolaRepository
    {
        Task<IEnumerable<Escola>> ListEscolaAsync();
        Escola GetById(int id);
        Escola Add(Escola person);
    }
}
