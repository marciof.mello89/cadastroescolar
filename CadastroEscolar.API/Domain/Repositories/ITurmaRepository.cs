﻿using CadastroEscolar.API.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CadastroEscolar.API.Domain.Repositories
{
    public interface ITurmaRepository
    {
        Task<IEnumerable<Turma>> ListTurmaAsync();
        Task<IEnumerable<Turma>> ListTurmaByEscolaAsync(int escolaId);
        Turma GetById(int id);
        Turma Add(Turma turma);
    }
}
