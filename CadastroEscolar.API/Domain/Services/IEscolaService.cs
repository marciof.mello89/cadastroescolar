﻿using CadastroEscolar.API.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CadastroEscolar.API.Domain.Services
{
    public interface IEscolaService
    {
        Escola Add(Escola person);
        Escola GetbyId(int id);
        Task<IEnumerable<Models.Escola>> ListEscolaAsync();
    }
}
