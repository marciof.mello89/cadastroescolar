﻿using CadastroEscolar.API.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CadastroEscolar.API.Domain.Services
{
    public interface ITurmaService
    {
        Turma Add(Turma turma);
        Turma GetbyId(int id);
        Task<IEnumerable<Turma>> ListTurmaAsync();
        Task<IEnumerable<Turma>> ListTurmaByEscolaAsync(int escolaId);
    }
}
