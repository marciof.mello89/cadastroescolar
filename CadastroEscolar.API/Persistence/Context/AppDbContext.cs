﻿using CadastroEscolar.API.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace CadastroEscolar.API.Persistence.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<Escola> Escola { get; set; }
        public DbSet<Turma> Turma { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Escola>().ToTable("Escolas");
            builder.Entity<Escola>().HasKey(p => p.Id);
            builder.Entity<Escola>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd().HasMaxLength(9);
            builder.Entity<Escola>().Property(p => p.Nome).IsRequired().HasMaxLength(50);
            builder.Entity<Escola>().HasMany(p => p.Turmas).WithOne(p => p.Escola).HasForeignKey(p => p.EscolaId);

            //builder.Entity<Escola>().HasData(
            //    LoadJsonData()
            //);

            builder.Entity<Turma>().ToTable("Turmas");
            builder.Entity<Turma>().HasKey(p => p.Id);
            builder.Entity<Turma>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd().HasMaxLength(9);
            builder.Entity<Turma>().Property(p => p.Nome).IsRequired().HasMaxLength(50);
        }
    }
}
