﻿using CadastroEscolar.API.Persistence.Context;

namespace CadastroEscolar.API.Persistence.Repositories
{
    public class BaseRepository
    {
        protected readonly AppDbContext _context;

        public BaseRepository(AppDbContext context)
        {
            _context = context;
        }
    }
}
