﻿using CadastroEscolar.API.Domain.Models;
using CadastroEscolar.API.Domain.Repositories;
using CadastroEscolar.API.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CadastroEscolar.API.Persistence.Repositories
{
    public class EscolaRepository : BaseRepository, IEscolaRepository
    {
        public EscolaRepository(AppDbContext context) : base(context)
        { }

        public async Task<IEnumerable<Escola>> ListEscolaAsync()
        {
            return await _context.Escola.ToListAsync();
        }

        public Escola GetById(int id)
        {
            try
            {
               return _context.Escola.Find(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Escola Add(Escola escola)
        {
            try
            {
                _context.Escola.Add(escola);
                _context.SaveChanges();

                return escola;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
