﻿using CadastroEscolar.API.Domain.Models;
using CadastroEscolar.API.Domain.Repositories;
using CadastroEscolar.API.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CadastroEscolar.API.Persistence.Repositories
{
    public class TurmaRepository : BaseRepository, ITurmaRepository
    {
        public TurmaRepository(AppDbContext context) : base(context)
        { }

        public async Task<IEnumerable<Turma>> ListTurmaAsync()
        {
            return await _context.Turma.ToListAsync();
        }

        public async Task<IEnumerable<Turma>> ListTurmaByEscolaAsync(int escolaId)
        {
            return await _context.Turma.Where(x => x.EscolaId == escolaId).ToListAsync();
        }

        public Turma GetById(int id)
        {
            try
            {
                return _context.Turma.Find(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Turma Add(Turma turma)
        {
            try
            {
                _context.Turma.Add(turma);
                _context.SaveChanges();

                return turma;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
