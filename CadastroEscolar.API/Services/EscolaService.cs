﻿using CadastroEscolar.API.Domain.Models;
using CadastroEscolar.API.Domain.Repositories;
using CadastroEscolar.API.Domain.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CadastroEscolar.API.Services
{
    public class EscolaService : IEscolaService
    {
        private readonly IEscolaRepository _escolaRepository;

        public EscolaService(IEscolaRepository personRepository)
        {
            _escolaRepository = personRepository;
        }

        public async Task<IEnumerable<Escola>> ListEscolaAsync()
        {
            return await _escolaRepository.ListEscolaAsync();
        }

        public Escola GetbyId(int id)
        {
            return _escolaRepository.GetById(id);
        }

        public Escola Add(Escola escola)
        {
            return _escolaRepository.Add(escola);
        }
    }
}
