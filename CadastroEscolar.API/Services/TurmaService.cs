﻿using CadastroEscolar.API.Domain.Models;
using CadastroEscolar.API.Domain.Repositories;
using CadastroEscolar.API.Domain.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CadastroEscolar.API.Services
{
    public class TurmaService : ITurmaService
    {
        private readonly ITurmaRepository _turmaRepository;

        public TurmaService(ITurmaRepository turmaRepository)
        {
            _turmaRepository = turmaRepository;
        }

        public async Task<IEnumerable<Turma>> ListTurmaAsync()
        {
            return await _turmaRepository.ListTurmaAsync();
        }

        public async Task<IEnumerable<Turma>> ListTurmaByEscolaAsync(int escolaId)
        {
            return await _turmaRepository.ListTurmaByEscolaAsync(escolaId);
        }

        public Turma GetbyId(int id)
        {
            return _turmaRepository.GetById(id);
        }

        public Turma Add(Turma turma)
        {
            return _turmaRepository.Add(turma);
        }
    }
}
