import { TurmaComponent } from './main/content/turma/turma.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EscolaComponent } from './main/content/escola/escola.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'app-escola',
    pathMatch: 'full'
  },
  {
    path: 'app-escola',
    component: EscolaComponent
},
{
  path: 'app-turma',
  component: TurmaComponent
},
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
