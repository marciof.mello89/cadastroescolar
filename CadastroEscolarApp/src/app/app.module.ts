import { EscolaModule } from './main/content/escola/escola.module';
import { ContentModule } from './main/content/content.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainModule } from './main/main.module';
import { SharedModule } from './main/content/shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CustomHttpInterceptor } from './main/content/shared/helper/loader.interceptor';
import { TurmaModule } from './main/content/turma/turma.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MainModule,
    ContentModule,
    SharedModule,
    AppRoutingModule,
    EscolaModule,
    TurmaModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS,  useClass: CustomHttpInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
