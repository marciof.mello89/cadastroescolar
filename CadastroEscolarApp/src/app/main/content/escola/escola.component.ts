import { EscolaService } from './../shared/services/escola.service';
import { Escola } from './../shared/models/escola.model';
import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { Util } from '../shared/util';
import { SpinnerService } from '../shared/services/spinner.service';
import { Router } from '@angular/router';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { AlertaToastrService } from '../shared/services/alertaToastr.service';
import { EscolaStore } from '../shared/store/escola.store';

@Component({
  selector: 'app-escola',
  templateUrl: './escola.component.html',
  styleUrls: ['./escola.component.scss']
})
export class EscolaComponent implements OnInit, AfterViewInit {
  form: FormGroup;
  escola: Escola = new Escola();
  displayedColumns: string[] = ['nome', 'acoes'];
  dataSource = new MatTableDataSource();
  resultsLength = 0;
  totalItems = 0;
  pageSize = 5;
  pageIndex = 0;
  pageEvent: PageEvent;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;


  constructor(
    private util: Util,
    public spinnerService: SpinnerService,
    private readonly router: Router,
    private formBuilder: FormBuilder,
    private cd: ChangeDetectorRef,
    private readonly escolaService: EscolaService,
    private toastrService: AlertaToastrService,
    private readonly escolaStore: EscolaStore) {
      this.carregaListaEscolas();
      this.escolaStore.update(undefined);
     }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      Escola: ['', Validators.required]
    });
  }

  ngAfterViewInit() {
    this.cd.detectChanges();
    this.dataSource.paginator = this.paginator;
  }

  carregaListaEscolas() {
    this.escolaService.getAll().subscribe(escolas => {
      this.dataSource.data = escolas;
      this.resultsLength = escolas.length;
      this.paginator.pageIndex = this.pageIndex;
      this.paginator.pageSize = this.pageSize;
      this.paginator.length = this.resultsLength;
      this.dataSource.paginator = this.paginator;
    });
  }

  cadastrarEscola() {
    this.validaCadastroEscola(resultado => {
      if (resultado) {
        this.escola.id = 0;
        this.escolaService.save(this.escola).subscribe(escola => {
          if (escola) {
            const mensagem = 'Escola cadastrada com sucesso!';
            this.toastrService.showToasterSuccess(mensagem, 'Sucesso');
            this.limparCampo();
            this.carregaListaEscolas();
          }
        }, erro => {
            const mensagem = erro;
            this.toastrService.showToasterError(mensagem, 'Erro ao cadastrar a escola!');
        });
      }
    });
  }

  validaCadastroEscola(callback) {
    if (this.util.IsNullOrUndefined(this.escola.nome)) {
      const mensagem = 'É necessário preencher o nome da escola!';
      this.toastrService.showToasterWarning(mensagem, 'Aviso');
      callback(false);
    } else {
      callback(true);
    }
  }

  visualizar(escola: Escola) {
    this.escolaStore.update(escola);
    this.router.navigate(['app-turma']);
  }

  limparCampo() {
    this.form.reset();
    this.escola = new Escola();
  }

}
