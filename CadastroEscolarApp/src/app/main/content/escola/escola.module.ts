import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EscolaComponent } from './escola.component';

@NgModule({
  declarations: [EscolaComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [EscolaComponent]
})
export class EscolaModule { }
