import { Escola } from './../models/escola.model';
import { Injectable } from '@angular/core';
import { ApiService } from './api.services';
import { BaseService } from 'src/app/core/base.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Util } from '../util';

@Injectable({
  providedIn: 'root'
})
export class EscolaService extends ApiService implements BaseService<Escola> {
  private strAction = 'Escola';

  constructor(
    httpClient: HttpClient,
    private http: HttpClient,
    private util: Util,
    private route: Router) {
    super(httpClient, route);
  }

  getAll(): Observable<Escola[]> {
    return this.get<Escola[]>(this.strAction);
  }

  deleteById(id: number) {
    throw new Error('Method not implemented.');
  }

  save(entity: Escola): Observable<Escola> {
    if (this.util.IsNullOrUndefined(entity.id) || entity.id === 0) {
      return this.post<Escola>(this.strAction, entity);
    }
    return this.put<Escola>(this.strAction, entity);
  }
}
