import { Injectable } from '@angular/core';
import { ApiService } from './api.services';
import { BaseService } from 'src/app/core/base.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Turma } from '../models/turma.model';
import { Util } from '../util';

@Injectable({
  providedIn: 'root'
})
export class TurmaService extends ApiService implements BaseService<Turma> {
  private strAction = 'Turma';

  constructor(
    httpClient: HttpClient,
    private http: HttpClient,
    private util: Util,
    private route: Router) {
    super(httpClient, route);
  }

  getAll(): Observable<Turma[]> {
    return this.get<Turma[]>(this.strAction);
  }

  getAllByEscola(escolaId: number): Observable<Turma[]> {
    return this.get<Turma[]>(this.strAction + `/Escola/${escolaId}`);
  }

  deleteById(id: number) {
    throw new Error('Method not implemented.');
  }

  save(entity: Turma): Observable<Turma> {
    if (this.util.IsNullOrUndefined(entity.id) || entity.id === 0) {
      return this.post<Turma>(this.strAction, entity);
    }
    return this.put<Turma>(this.strAction, entity);
  }
}
