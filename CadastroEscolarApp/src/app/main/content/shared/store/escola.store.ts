import { Escola } from './../models/escola.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class EscolaStore {

    private Subject = new BehaviorSubject<Escola>(null);
    escola = this.Subject.asObservable();

    constructor() { }

    getCurrent(): Observable<Escola> {
        return this.escola;
    }

    update(escola: Escola) {
        this.Subject.next(escola);
    }
}
