import { Turma } from './../models/Turma.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class TurmaStore {

    private Subject = new BehaviorSubject<Turma>(null);
    turma = this.Subject.asObservable();

    constructor() { }

    getCurrent(): Observable<Turma> {
        return this.turma;
    }

    update(turma: Turma) {
        this.Subject.next(turma);
    }
}
