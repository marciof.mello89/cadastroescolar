import { Escola } from './../shared/models/escola.model';
import { Component, OnInit, ViewChild, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Turma } from '../shared/models/Turma.model';
import { MatTableDataSource } from '@angular/material/table';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { Util } from '../shared/util';
import { SpinnerService } from '../shared/services/spinner.service';
import { Router } from '@angular/router';
import { TurmaService } from '../shared/services/turma.service';
import { AlertaToastrService } from '../shared/services/alertaToastr.service';
import { EscolaService } from '../shared/services/escola.service';
import { EscolaStore } from '../shared/store/escola.store';
import { error } from 'protractor';

@Component({
  selector: 'app-turma',
  templateUrl: './turma.component.html',
  styleUrls: ['./turma.component.scss']
})
export class TurmaComponent implements OnInit, AfterViewInit {
  form: FormGroup;
  turma: Turma = new Turma();
  escolas: Escola[] = [];
  displayedColumns: string[] = ['nome'];
  dataSource = new MatTableDataSource();
  resultsLength = 0;
  totalItems = 0;
  pageSize = 5;
  pageIndex = 0;
  pageEvent: PageEvent;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(
    private util: Util,
    public spinnerService: SpinnerService,
    private readonly router: Router,
    private formBuilder: FormBuilder,
    private cd: ChangeDetectorRef,
    private readonly turmaService: TurmaService,
    private readonly escolaService: EscolaService,
    private toastrService: AlertaToastrService,
    private readonly escolaStore: EscolaStore) {
      this.carregaListaEscolas();
      this.escolaStore.getCurrent().subscribe(escola => {
        if (escola) {
          this.turma.escolaId = escola.id;
          this.carregaListaTurmasPorEscola(this.turma.escolaId);
        } else {
          this.carregaListaTurmas();
        }
      });
     }

    ngOnInit(): void {
      this.form = this.formBuilder.group({
        Turma: ['', Validators.required],
        Escola: ['', Validators.required]
      });
    }

    ngAfterViewInit() {
      this.cd.detectChanges();
      this.dataSource.paginator = this.paginator;
    }

    carregaListaTurmas() {
      this.turmaService.getAll().subscribe(turmas => {
        this.dataSource.data = turmas;
        this.resultsLength = turmas.length;
        this.paginator.pageIndex = this.pageIndex;
        this.paginator.pageSize = this.pageSize;
        this.paginator.length = this.resultsLength;
        this.dataSource.paginator = this.paginator;
      });
    }

    carregaListaTurmasPorEscola(escola: number) {
      this.turmaService.getAllByEscola(escola).subscribe(turmas => {
        if (turmas) {
        this.dataSource.data = turmas;
        this.resultsLength = turmas.length;
        this.paginator.pageIndex = this.pageIndex;
        this.paginator.pageSize = this.pageSize;
        this.paginator.length = this.resultsLength;
        this.dataSource.paginator = this.paginator;
      } else {
        this.dataSource.data = undefined;
      }
      });
    }

    carregaListaEscolas() {
      this.escolaService.getAll().subscribe(escolas => {
        if (escolas.length > 0) {
          this.escolas = escolas;
        } else {
          const message = 'Não foram encontradas escolas cadastradas.';
          this.toastrService.showToasterWarning(message, 'Cadastro de turma indisponível!');
        }
      }, erro => {
          const message = erro;
          this.toastrService.showToasterWarning(message, 'Erro ao recuperar lista de escolas!');
      });
    }

  cadastrarTurma() {
    this.validaCadastroTurma(resultado => {
      if (resultado) {
        this.turma.id = 0;
        this.turmaService.save(this.turma).subscribe(turma => {
          if (turma) {
            const mensagem = 'Turma cadastrada com sucesso!';
            this.toastrService.showToasterSuccess(mensagem, 'Sucesso');
            this.carregaListaTurmasPorEscola(this.turma.escolaId);

            this.turma.nome = '';
            this.formularioTurma.Turma.setValue('');
          }
        }, error => {
            const mensagem = 'Ocorreu um erro ao cadastrar a turma! Erro: ' + error;
            this.toastrService.showToasterError(mensagem, 'Erro');
        });
      }
    });
  }

  validaCadastroTurma(callback) {
    if (this.util.IsNullOrUndefined(this.turma.nome)) {
      const mensagem = 'É necessário preencher o nome da turma!';
      this.toastrService.showToasterWarning(mensagem, 'Aviso');
      callback(false);
    } else if (this.util.IsNullOrUndefined(this.turma.escolaId)) {
      const mensagem = 'É necessário selecionar uma escola!';
      this.toastrService.showToasterWarning(mensagem, 'Aviso');
      callback(false);
    } else {
      callback(true);
    }
  }

  limparCampos() {
    this.form.reset();
    this.turma = new Turma();
  }

  voltar(): void {
    this.limparCampos();
    this.router.navigate(['app-escola']);
  }

  get formularioTurma() {
      return this.form.controls;
  }
}
