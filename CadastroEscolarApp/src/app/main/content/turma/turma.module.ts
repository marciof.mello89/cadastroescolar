import { SharedModule } from './../shared/shared.module';
import { TurmaComponent } from './turma.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [TurmaComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [TurmaComponent]
})
export class TurmaModule { }
